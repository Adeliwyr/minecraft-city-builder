# Minecraft City Builder

Ce projet à pour but de faciliter la création de cité pour minecraft.

Le serveur se base actuellement sur l'architecture Paper sur la version 1.17. Tout l'environnement est dockerisé pour permettre une plus grande réusabilité du projet.

Voici la liste des plugins installés:
 
* [AdvancedAchievements](https://www.spigotmc.org/resources/advanced-achievements.83466/)
* [AntiCooldown](https://www.spigotmc.org/resources/%E2%AD%90v4-release%E2%AD%90anticooldown-1-9-1-17-1.51321/)
* [ATopPlayers](https://www.spigotmc.org/resources/atopplayers-create-leaderboards-for-kills-deaths-time-diamond-e-t-c.93996/)
* [Chunky](https://www.spigotmc.org/resources/chunky.81534/)
* [Citizen](https://www.spigotmc.org/resources/citizens.13811/)
* [DropHeads](https://dev.bukkit.org/projects/dropheads)
* [EconomyShopGUI](https://www.spigotmc.org/resources/economyshopgui.69927/)
* [EssentialsX](https://www.spigotmc.org/resources/essentialsx.9089/)
* [FAWE](https://www.spigotmc.org/resources/fast-async-worldedit.13932/)
* [LuckPerms](https://www.spigotmc.org/resources/luckperms.28140/)
* [MoneyFromMobs](https://www.spigotmc.org/resources/money-from-mobs-1-12-1-17.79137/)
* [PlaceholderAPI](https://www.spigotmc.org/resources/placeholderapi.6245/)
* [Vault](https://www.spigotmc.org/resources/vault.34315/)
* [WorldGuard](https://dev.bukkit.org/projects/worldguard)

## Installation

Démarrer le serveur:

```shell
$ make deploy
```

Fermer le serveur:

```shell
$ make undeploy
```

Ouvrir la console:

```shell
$ make console
```

Consulter les logs:

```shell
$ make logs
```

## Initialiser une cité

1 - Construire/remplacer les cartes du serveur (world, world_nether, world_the_end)  
2 - Prégénérer les chunks et la world border vanilla (exemple avec un radius de 1000 => carte en 2000x2000)

```
$ make console
> worldborder center 0 0
> worldborder set 1000
> chunky worldborder
> chunky start
```

3 - Sécuriser la cité à l'aide d'une region WorldGuard

* Sélectionner une zone avec le //wand 
* //expand up 255
* //expand down 255
* /region define city
* Ajouter les flags suivants sur la region worldguard dans les fichiers de configuration `(plugins/WorldGuard/worlds/world/regions.yml)`

```yml
flags: {other-explosion: deny, deny-message: 'Désolé, tu n''es pas autorisé
                à faire ça ici.', frosted-ice-form: deny, lava-fire: deny, use: deny,
            greeting: '&aVous sentez l''énergie de la cité vous apaiser.', leaf-decay: deny,
            ice-melt: deny, block-trampling: deny, snow-fall: deny, interact: allow,
            chest-access: allow, firework-damage: deny, fire-spread: deny, enderdragon-block-damage: deny,
            sleep: allow, snowman-trails: deny, feed-delay: 5, mob-damage: deny, ravager-grief: deny,
            entity-painting-destroy: deny, mushroom-growth: allow, lightning: deny,
            wither-damage: deny, ice-form: deny, chorus-fruit-teleport: deny, feed-amount: 5,
            lighter: deny, enderman-grief: deny, pistons: deny, pvp: deny, mob-spawning: deny,
            enderpearl: deny, creeper-explosion: deny, vine-growth: deny, item-frame-rotation: deny,
            potion-splash: deny, damage-animals: deny, frosted-ice-melt: deny, snow-melt: deny,
            tnt: deny, ghast-fireball: deny, entity-item-frame-destroy: deny, allow-shop: allow,
            farewell: '&cLa cité n''a plus aucun effet de protection sur vous.'}
```

4 - Ajouter les PNJ/Shops dans la cité

Voir [Shops](./docs/shops.md).

5 - Ajouter des coffres (secrets) dans la cité

6 - Réinitiliser toutes les statistiques des joueurs

* Supprimer les bases de données dans [AdvancedAchievements](./paper/plugins/AdvancedAchievements)
* Supprimer la base de donnée dans [ATopPlayers](./paper/plugins/ATopPlayers)
$ Supprimer le dossier playerdate dans le dossier world
* /advancement revoke @a everything
* /aach delete * @a

# TODO

#### Optimisation

- [ ] Passer les BDD SQLlite & h2 (file) sur des bases de données plus optimisées (à tester) 

* AdvancedAdvancement
* ATopPlayer
* LuckPerms

#### Shops

- [ ] Réorganiser les shops
- [ ] Ajouter des items (fun/cheat) sur les PNJ (ex: Fortune 4/5, Houe 3x3, etc ...)

#### Equipes

- [ ] Ajouter un plugin pour la gestion d'équipes

#### Permissions

- [ ] Ajouter les permissions pour certaines commandes
- [ ] Ajouter le moyen d'acheter des commandes/avantages pour de l'argent virtuel

#### Scoreboard

- [ ] Ajouter un système détaillé pour le scoreboard/chat/tabs

#### Traductions

- [ ] plugin/AdvancesAchievements/lang.yml
- [ ] plugin/AntiCooldown/config.yml

#### Achievements

- [ ] Refaire les achievement/récompenses (réequilibrage)

#### Events

- [ ] Ajouter des plugins pour l'organisation des évènements
- [ ] Ajout des script pour la modification dynamique de certains plugins (ex: prix x2, kill x2 , etc ...)
- [ ] Réquilibrer les récompenses des évènements

#### Monitoring

- [ ] Mettre en place une application web pour monitorer le serveur en ligne (+ accès console)

#### Bugs

- [ ] Corriger le bug de récupération de spawners par les joueurs
- [ ] Error occurred while enabling AntiCooldown v4.0 (Pourtant le plugin fonctionne)