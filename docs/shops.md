# Créer un PNJ shop

```
# Créer le PNJ
/npc create [NAME]

# Sélectionner un PNJ
/npc list
/npc select [ID/NAME]
/npc remove [ID/NAME]

# Renomer un PNJ
/npc rename [NAME]

# Modifier l'apparence du PNJ
/npc skin [PLAYER_NAME]
/npc skin --url https://mineskin.org/gallery/1
/npc size 2
/npc equip

# Repositioner le PNJ
/npc tphere

# Ajouter un shop à un PNJ
/npc command add shop [SECTION] <p>
```

# Editer le shop

Pour configurer les sections: 

```
plugins/EconomyShopGUI/sections.yml
```

Pour ce qui est afficher dans le /show [SECTION]

```
plugins/EconomyShopGUI/shops.yml
```

# Credits

* https://wiki.citizensnpcs.co/Commands
* https://gpplugins.gitbook.io/economyshopgui/basics/how-to#how-to-make-an-npc-shop